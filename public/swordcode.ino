#include <Adafruit_NeoPixel.h> //include LED library

    #define PIN  8              //LED PIN
    #define NUMPIXELS      30   //Number of LEDs


    // defines pins numbers of sensor
    const int trigPin = 10;
    const int echoPin = 9;


    // defines variables for calculation
    long duration;
    float distance;

    Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800); //LED output variable


    void setup() {

    pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
    pinMode(echoPin, INPUT); // Sets the echoPin as an Input
    pixels.begin(); // This initializes the NeoPixel library.
    Serial.begin(9600); // Starts the serial communication
    }
    void loop() {
      
    // Clears the trigPin
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    distance= duration*0.034/2;
    // If statement to light up LED only if hand is in front of it
   if(distance <4 && distance >0){
    for(int i=0;i<NUMPIXELS;i++){

    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(0,150,0)); // Moderately bright green color.

    pixels.show(); // This sends the updated pixel color to the hardware.

    }
delay(10000);//light up for 10 seconds
   }else{    //If nothing is recognised turn the light off. could have been empty but for good measure i specifically said to turn the lights off.
    for(int i=0;i<NUMPIXELS;i++){

    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(0,0,0)); // no light.
        pixels.show(); // This sends the updated pixel color to the hardware.
   }

   }}
